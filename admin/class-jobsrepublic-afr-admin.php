<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.jobsrepublic.nl
 * @since      1.0.0
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/admin
 * @author     Jobsrepublic <info@jobsrepublic.nl>
 */
class Jobsrepublic_Afr_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Jobsrepublic_Afr_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Jobsrepublic_Afr_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/jobsrepublic-afr-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Jobsrepublic_Afr_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Jobsrepublic_Afr_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/jobsrepublic-afr-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Include admin options page into WP flow
	 *
	 */
	public function admin_options_page() {
		add_options_page(
			__( 'Jobsrepublic Async Routing Options', 'jobstoo' ),
			__( 'Jobsrepublic Async Routing Options', 'jobstoo' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'render_options_page' ),
		);
	}

	/**
	 * Render options page
	 */
	public function render_options_page() {

		include plugin_dir_path( __FILE__ ) . 'partials/jobsrepublic-afr-admin-display.php';

	}

	/**
	 * Updates options.
	 */
	public function options_update() {
		register_setting( $this->plugin_name, $this->plugin_name, array( $this, 'options_validate' ) );
	}

	/**
	 * Validates options
	 */
	public function options_validate( $options ) {
		$valid = array();

		$valid['access-token'] = sanitize_text_field( $options['access-token'] );

		return $valid;

	}

}
