<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.jobsrepublic.nl
 * @since      1.0.0
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 * @author     Jobsrepublic <info@jobsrepublic.nl>
 */
class Jobsrepublic_Afr {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Jobsrepublic_Afr_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'JOBSREPUBLIC_AFR_VERSION' ) ) {
			$this->version = JOBSREPUBLIC_AFR_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'jobsrepublic-afr';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->hook_wp_json_routes();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Jobsrepublic_Afr_Loader. Orchestrates the hooks of the plugin.
	 * - Jobsrepublic_Afr_i18n. Defines internationalization functionality.
	 * - Jobsrepublic_Afr_Admin. Defines all hooks for the admin area.
	 * - Jobsrepublic_Afr_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-jobsrepublic-afr-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-jobsrepublic-afr-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-jobsrepublic-afr-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-jobsrepublic-afr-public.php';

		$this->loader = new Jobsrepublic_Afr_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Jobsrepublic_Afr_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Jobsrepublic_Afr_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Jobsrepublic_Afr_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'admin_options_page' );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'options_update' ); // Save/update options.

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Jobsrepublic_Afr_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Jobsrepublic_Afr_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Hooks wp-json routes
	 *
	 * @return void
	 */
	private function hook_wp_json_routes() {

		add_action( 'rest_api_init', array( $this, 'register_inbound_route' ) );
		add_action( 'rest_api_init', array( $this, 'register_outbound_route' ) );

	}

	/**
	 * Register the inbound endpoint
	 *
	 * @return void
	 */
	public function register_inbound_route() {
		register_rest_route(
			'async-form-routing',
			'/inbound',
			array(
				array(
					'methods'  => 'POST',
					'callback' => array( $this, 'process_async_inbound' ),
					'args' => array(
						'stepId' => array(
							'default' => null,
							'required' => true,
						),
						'userId' => array(
							'default' => null,
							'required' => true,
						),
						'redirectUrl' => array(
							'default' => null,
							'required' => true,
						),
						'accessToken' => array(
							'default' => null,
							'required' => true,
						),
					),
				),
			)
		);
	}

	/**
	 * Register the outbound endpoint
	 *
	 * @return void
	 */
	public function register_outbound_route() {
		register_rest_route(
			'async-form-routing',
			'/outbound',
			array(
				array(
					'methods'  => 'GET',
					'callback' => array( $this, 'process_async_outbound' ),
					'args' => array(
						'stepId' => array(
							'default' => null,
							'required' => true,
						),
						'userId' => array(
							'default' => null,
							'required' => true,
						),
					),
				),
			)
		);
	}

	/**
	 * Process inbound async request
	 *
	 * @param WP_REST_Request request object
	 * @return array|WP_REST_Request|WP_ERROR $response with response values
	 */
	public function process_async_inbound( WP_REST_Request $request ) {

		$body = $request->get_json_params();
		//error_log ( print_r ( $request, true ) );
		if ( $body['accessToken'] != get_option( $this->plugin_name )['access-token'] ){
			return 0; // This is quick escape from the function execution. Needs refining.
		}

		$result = set_transient( $body['stepId'] . '_' . $body['userId'], $body['redirectUrl'], 60*30 );

		$response = rest_ensure_response( array( 'result' => ( true === $result ? 'success' : 'failure' )  ) ); 

		$response->set_status( 200 );

		return $response;

	}

	/**
	 * Process outbound async request
	 *
	 * @param WP_REST_Request request object
	 * @return array|WP_REST_Request|WP_ERROR $response with response values
	 */
	public function process_async_outbound( WP_REST_Request $request ) {

		$body = $request->get_params();

		$redirect_email = get_transient( $body['stepId'] . '_' . $body['userId'] ) ;

		$response = rest_ensure_response( array( 'redirectUrl'=> ( false === $redirect_email ? 'null' : $redirect_email )  ) ); 

		$response->set_status( 200 );

		return $response;
	}

}
