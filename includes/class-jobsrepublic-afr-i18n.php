<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.jobsrepublic.nl
 * @since      1.0.0
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 * @author     Jobsrepublic <info@jobsrepublic.nl>
 */
class Jobsrepublic_Afr_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'jobsrepublic-afr',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
