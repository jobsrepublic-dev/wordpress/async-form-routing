<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.jobsrepublic.nl
 * @since      1.0.0
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 * @author     Jobsrepublic <info@jobsrepublic.nl>
 */
class Jobsrepublic_Afr_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
	}

}
