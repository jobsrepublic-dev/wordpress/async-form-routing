<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.jobsrepublic.nl
 * @since      1.0.0
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/includes
 * @author     Jobsrepublic <info@jobsrepublic.nl>
 */
class Jobsrepublic_Afr_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		/* delete_option( 'jobsrepublic_afr_access_token' ); */
	}

}
