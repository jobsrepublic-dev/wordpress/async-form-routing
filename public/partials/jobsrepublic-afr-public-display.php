<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.jobsrepublic.nl
 * @since      1.0.0
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
