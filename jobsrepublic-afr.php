<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.jobsrepublic.nl
 * @since             1.0.0
 * @package           Jobsrepublic_Afr
 *
 * @wordpress-plugin
 * Plugin Name:       Jobsrepublic Async Form Routing
 * Plugin URI:        http://www.jobsrepublic.nl
 * GitLab Plugin URI: https://gitlab.com/jobsrepublic-dev/wordpress/async-form-routing
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.1
 * Author:            Jobsrepublic
 * Author URI:        http://www.jobsrepublic.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       jobsrepublic-afr
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'JOBSREPUBLIC_AFR_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-jobsrepublic-afr-activator.php
 */
function activate_jobsrepublic_afr() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-jobsrepublic-afr-activator.php';
	Jobsrepublic_Afr_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-jobsrepublic-afr-deactivator.php
 */
function deactivate_jobsrepublic_afr() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-jobsrepublic-afr-deactivator.php';
	Jobsrepublic_Afr_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_jobsrepublic_afr' );
register_deactivation_hook( __FILE__, 'deactivate_jobsrepublic_afr' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-jobsrepublic-afr.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_jobsrepublic_afr() {

	$plugin = new Jobsrepublic_Afr();
	$plugin->run();

}
run_jobsrepublic_afr();
