<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.jobsrepublic.nl
 * @since      1.0.0
 *
 * @package    Jobsrepublic_Afr
 * @subpackage Jobsrepublic_Afr/admin/partials
 */

$options = get_option( $this->plugin_name );
/*
echo '<pre>';
var_dump($options);
echo '</pre>';
*/
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

		<form method="post" name="<?php echo $this->plugin_name ?>_options" action="options.php">

			<?php settings_fields( $this->plugin_name ); ?>
			<?php do_settings_sections( $this->plugin_name ); ?>

			<h2><?php esc_attr_e( 'Jobsrepublic Async Routing settings', 'jobsrepublic-afr' ); ?></h2>
			<h3><?php esc_attr_e( 'Channel token', 'jobsrepublic-afr' ); ?></h3>
			<fieldset>
				<legend class="screen-reader-text"><span><?php esc_attr_e( 'Access token', 'jobsrepublic-afr' ); ?></span></legend>
				<label for="<?php echo $this->plugin_name; ?>-access-token">
					<input type="text" id="<?php echo $this->plugin_name; ?>-access-token" name="<?php echo esc_html( $this->plugin_name ); ?>[access-token]"<?php echo $options['access-token'] ? ' value="' . $options['access-token'] . '"' : '' ; ?>/>
					<span><?php esc_attr_e( 'Access token used for submitting transient data.', 'jobsrepublic-afr' ); ?></span>
				</label>
			</fieldset>

			<?php submit_button( 'Update settings', 'primary', 'submit', true ); ?>

	</form>

</div>
